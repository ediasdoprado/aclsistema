﻿namespace ACLSistema.Infra.Helpers
{
    public static class HelperString
    {
        /// <summary>
        /// Remove caracteres especiais.
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RemoveCaracteresEspeciais(string texto) {
                        
            /** Troca os caracteres acentuados por não acentuados **/
            string[] acentos = new string[] { "ç", "Ç", "á", "é", "í", "ó", "ú", "ý", "Á", "É", "Í", "Ó", "Ú", "Ý", "à", "è", "ì", "ò", "ù", "À", "È", "Ì", "Ò", "Ù", "ã", "õ", "ñ", "ä", "ë", "ï", "ö", "ü", "ÿ", "Ä", "Ë", "Ï", "Ö", "Ü", "Ã", "Õ", "Ñ", "â", "ê", "î", "ô", "û", "Â", "Ê", "Î", "Ô", "Û" };
            string[] semAcento = new string[] { "c", "C", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "a", "o", "n", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "A", "O", "N", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U" };

            for (int i = 0; i < acentos.Length; i++)
            {
                texto = texto.Replace(acentos[i], semAcento[i]);
            }
            /** Troca os caracteres especiais da string por "" **/
            string[] caracteresEspeciais = { "\\.", ",", "-", ":", "\\(", "\\)", "ª", "\\|", "\\\\", "°" };

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                texto = texto.Replace(caracteresEspeciais[i], "");
            }

            /** Troca os espaços no início por "" **/
            texto = texto.Replace("^\\s+", "");
            /** Troca os espaços no início por "" **/
            texto = texto.Replace("\\s+$", "");
            /** Troca os espaços duplicados, tabulações e etc por " " **/
            texto = texto.Replace("\\s+", " ");

            return texto;
        }

        /// <summary>
        /// Remove caracteres e retorna um texto maiusculo.
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RemoveCaracteresToUpper(string texto) {

            return RemoveCaracteresEspeciais(texto).ToUpper();
        }

        /// <summary>
        /// Remove caracteres e retorna um texto minusculo.
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RemoveCaracteresToLower(string texto)
        {
            return RemoveCaracteresEspeciais(texto).ToLower();
        }

    }
}
