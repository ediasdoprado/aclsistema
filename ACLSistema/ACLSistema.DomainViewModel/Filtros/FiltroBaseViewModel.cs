﻿using System;

namespace ACLSistema.DomainViewModel.Filtros
{
    /// <summary>
    /// Filtro base herdado por diversas entidades.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class FiltroBaseViewModel
    {
        public int registros { get; set; }

        public int pagina { get; set; }

        public bool? ativo { get; set; }

        public Guid? codigo { get; set; }
    }
}
