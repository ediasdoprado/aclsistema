﻿using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.Infra;
using FluentValidation;

namespace ACLSistema.DomainViewModel.Validacoes
{
    public class ModuloViewModelValidador : AbstractValidator<ModuloViewModel>
    {
        public ModuloViewModelValidador()
        {
            RuleFor(x => x.Nome)
              .NotEmpty().WithMessage(string.Format(Resource.CampoObrigatorio, Resource.Nome));
        }
    }
}
