﻿using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.Infra;
using FluentValidation;

namespace ACLSistema.DomainViewModel.Validacoes
{
    public class GrupoUsuarioViewModelValidador : AbstractValidator<GrupoUsuarioViewModel>
    {
        public GrupoUsuarioViewModelValidador()
        {
            RuleFor(x => x.Nome)
              .NotEmpty().WithMessage(string.Format(Resource.CampoObrigatorio, Resource.Nome));
        }
    }
}
