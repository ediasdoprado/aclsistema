﻿using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.Infra;
using FluentValidation;

namespace ACLSistema.DomainViewModel.Validacoes
{
    public class FuncionalidadeViewModelValidador: AbstractValidator<FuncionalidadeViewModel>
    {
        public FuncionalidadeViewModelValidador()
        {
            RuleFor(x => x.Nome)
             .NotEmpty().WithMessage(string.Format(Resource.CampoObrigatorio, Resource.Nome));

            RuleFor(x => x.ModuloId)
             .NotEmpty().WithMessage(string.Format(Resource.CampoObrigatorio, Resource.Modulo));
        }
    }
}
