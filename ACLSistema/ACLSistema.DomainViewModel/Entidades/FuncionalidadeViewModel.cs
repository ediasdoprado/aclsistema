﻿
using ACLSistema.DomainViewModel.Validacoes;

namespace ACLSistema.DomainViewModel.Entidades
{
    /// <summary>
    /// Funcionalidades da area administrativa do sistema.
    /// </summary>
    [FluentValidation.Attributes.Validator(typeof(FuncionalidadeViewModelValidador))]
    public class FuncionalidadeViewModel : EntidadeBaseViewModel
    {
        public string Nome { get; set; }

        public string Icone { get; set; }

        public long ModuloId { get; set; }

        public virtual ModuloViewModel Modulo { get; set; }
    }
}
