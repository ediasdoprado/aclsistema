﻿using ACLSistema.DomainViewModel.Validacoes;
using System.Collections.Generic;

namespace ACLSistema.DomainViewModel.Entidades
{
    /// <summary>
    /// Modulos da area administrativa
    /// </summary>
    [FluentValidation.Attributes.Validator(typeof(ModuloViewModelValidador))]
    public class ModuloViewModel : EntidadeBaseViewModel
    {
        public string Nome { get; set; }

        public string Icone { get; set; }

        public virtual List<FuncionalidadeViewModel> Funcionalidades { get; set; }
    }
}
