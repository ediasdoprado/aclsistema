﻿using System.Collections.Generic;

namespace ACLSistema.DomainViewModel.Entidades
{
    public class GruposUsuariosModulosViewModel
    {
        public long GrupoUsuarioId { get; set; }

        public long ModuloId { get; set; }

        public virtual List<ModuloViewModel> Modulos { get; set; }
    }
}
