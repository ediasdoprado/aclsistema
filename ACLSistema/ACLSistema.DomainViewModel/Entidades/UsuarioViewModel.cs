﻿
namespace ACLSistema.DomainViewModel.Entidades
{
    public class UsuarioViewModel : EntidadeBaseViewModel
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string CPF { get; set; }

        public string Senha { get; set; }

        public long GrupoUsuarioId { get; set; }

        public virtual GrupoUsuarioViewModel GrupoUsuario { get; set; }
    }
}
