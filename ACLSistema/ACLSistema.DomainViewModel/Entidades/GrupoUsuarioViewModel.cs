﻿using ACLSistema.DomainViewModel.Validacoes;
using System.Collections.Generic;

namespace ACLSistema.DomainViewModel.Entidades
{
    [FluentValidation.Attributes.Validator(typeof(GrupoUsuarioViewModelValidador))]
    public class GrupoUsuarioViewModel : EntidadeBaseViewModel
    {
        public string Nome { get; set; }

        public List<UsuarioViewModel> Usuarios { get; set; }

        //public List<GruposUsuariosModulosViewModel> GruposUsuariosModulos { get; set; }
    }
}
