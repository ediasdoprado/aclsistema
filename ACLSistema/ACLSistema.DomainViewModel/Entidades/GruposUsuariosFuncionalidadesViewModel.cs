﻿using System.Collections.Generic;

namespace ACLSistema.DomainViewModel.Entidades
{
    public class GruposUsuariosFuncionalidadesViewModel
    {
        public long GrupoUsuarioId { get; set; }

        public long FuncionalidadeId { get; set; }

        public virtual List<FuncionalidadeViewModel> Funcionalidades { get; set; }
    }
}
