﻿using System;
using System.Linq.Expressions;

namespace ACLSistema.Domain.Filtros
{
    /// <summary>
    /// Filtro base herdado por diversas entidades.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class FiltroBase<TEntity> where TEntity : class
    {
        public int registros { get; set; }

        public int pagina { get; set; }

        public bool? ativo { get; set; }
        
        public Guid? codigo { get; set; }

        //public DateTime? cadastro { get; set; }

        //public DateTime? atualizacao { get; set; }

        //public DateTime? exclusao { get; set; }

        public virtual Expression<Func<TEntity, bool>> predicate { get; }
    }
}
