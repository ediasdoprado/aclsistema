﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Infra.Helpers;
using System;
using System.Linq.Expressions;

namespace ACLSistema.Domain.Filtros
{
    public class FiltroGrupoUsuario : FiltroBase<GrupoUsuario>
    {
        public string nome { get; set; }

        public override Expression<Func<GrupoUsuario, bool>> predicate
        {
            get
            {
                return (u =>
                    (string.IsNullOrEmpty(nome) || HelperString.RemoveCaracteresToLower(u.Nome).Contains(HelperString.RemoveCaracteresToLower(nome)))
                    && (codigo == null || u.Codigo == codigo)
                    && (ativo == null || u.Ativo == ativo)
                    && (u.Excluido == false)
                );
            }
        }

        /// <summary>
        /// Construtor com parametro para trazer um unico registro
        /// </summary>
        /// <param name="texto"></param>
        public FiltroGrupoUsuario(string texto)
        {
            nome = texto;
            registros = 1;
            pagina = 1;
        }

        /// <summary>
        /// Construtor sem parametro
        /// </summary>
        public FiltroGrupoUsuario()
        {

        }
    }
}
