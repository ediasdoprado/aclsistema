﻿using System.Collections.Generic;

namespace ACLSistema.Domain.Entidades
{
    /// <summary>
    /// Modulos da area administrativa
    /// </summary>
    public class Modulo : EntidadeBase 
    {
        public string Nome { get; set; }

        public string Icone { get; set; }

        public virtual List<Funcionalidade> Funcionalidades { get; set; }
    }
}
