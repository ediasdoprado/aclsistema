﻿using System.Collections.Generic;

namespace ACLSistema.Domain.Entidades
{
    public class GruposUsuariosModulos
    {
        public long GrupoUsuarioId { get; set; }
        
        public long ModuloId { get; set; }

        public virtual List<Modulo> Modulos { get; set; }
    }
}
