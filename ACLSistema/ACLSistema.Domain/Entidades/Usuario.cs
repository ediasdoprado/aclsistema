﻿namespace ACLSistema.Domain.Entidades
{
    public class Usuario : EntidadeBase
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string CPF { get; set; }

        public string Senha { get; set; }

        public long GrupoUsuarioId { get; set; }

        public virtual GrupoUsuario GrupoUsuario { get; set; }
    }
}
