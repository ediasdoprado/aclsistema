﻿using System.Collections.Generic;

namespace ACLSistema.Domain.Entidades
{
    public class GruposUsuariosFuncionalidades
    {
        public long GrupoUsuarioId { get; set; }

        public long FuncionalidadeId { get; set; }

        public virtual List<Funcionalidade> Funcionalidades { get; set; }
    }
}
