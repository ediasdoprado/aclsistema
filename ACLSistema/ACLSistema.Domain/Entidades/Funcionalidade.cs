﻿
namespace ACLSistema.Domain.Entidades
{
    /// <summary>
    /// Funcionalidades da area administrativa do sistema.
    /// </summary>
    public class Funcionalidade : EntidadeBase
    {
        public string Nome { get; set; }

        public string Icone { get; set; }

        public long ModuloId { get; set; }

        public virtual Modulo Modulo { get; set; }
    }
}
