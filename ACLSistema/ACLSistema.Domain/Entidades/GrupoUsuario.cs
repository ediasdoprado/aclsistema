﻿
using System.Collections.Generic;

namespace ACLSistema.Domain.Entidades
{
    /// <summary>
    /// Grupos de usuarios da area administrativa
    /// </summary>
    public class GrupoUsuario : EntidadeBase
    {
        public string Nome { get; set; }

        public List<Usuario> Usuarios { get; set; }

        //public List<GruposUsuariosModulos> GruposUsuariosModulos { get; set; }
    }
}
