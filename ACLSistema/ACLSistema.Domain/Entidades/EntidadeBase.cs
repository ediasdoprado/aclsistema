﻿using System;

namespace ACLSistema.Domain.Entidades
{
    /// <summary>
    /// Entidade que deverá ser herdada por todas as demais.
    /// </summary>
    public abstract class EntidadeBase
    {
        public long Id { get; set; }

        public Guid Codigo { get; set; }

        public DateTime Cadastro { get; set; }

        public DateTime? Atualizacao { get; set; }

        public DateTime? Exclusao { get; set; }

        public bool Excluido { get; set; }

        public bool Ativo { get; set; }
    }
}
