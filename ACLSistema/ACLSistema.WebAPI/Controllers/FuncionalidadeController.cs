﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.DomainViewModel.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using ACLSistema.WebAPI.Filters;
using ACLSistema.WebAPI.Models.Exceptions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ACLSistema.WebAPI.Controllers
{
    public class FuncionalidadeController : ApiController
    {
        private readonly IFuncionalidadeService _funcionalidadeService;

        public FuncionalidadeController(IFuncionalidadeService funcionalidadeService)
        {
            _funcionalidadeService = funcionalidadeService;
        }

        /// <summary>
        /// Retorna uma lista de registros a partir de um filtro
        /// </summary>
        /// <param name="filtroViewModel"></param>
        /// <returns></returns>
        public List<FuncionalidadeViewModel> Get([FromUri]FiltroFuncionalidadeViewModel filtroViewModel)
        {
            try
            {
                var filtro = Mapper.Map<FiltroFuncionalidadeViewModel, FiltroFuncionalidade>(filtroViewModel);

                var funcionalidades = _funcionalidadeService.GetByFilter(filtro);

                var funcionalidadesViewModel = Mapper.Map<List<Funcionalidade>, List<FuncionalidadeViewModel>>(funcionalidades);

                if (funcionalidades.Count == 0)
                {
                    string message = string.Format(Resource.NaoEncontradosParametros, Resource.Funcionalidades);
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }

                return funcionalidadesViewModel;
            }
            catch (RequisicaoException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Retorna um registro a partir do parametro informado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public FuncionalidadeViewModel Get(long id)
        {
            try
            {

                var funcionalidade = _funcionalidadeService.GetById(id);
                var funcionalidadeViewModel = Mapper.Map<Funcionalidade, FuncionalidadeViewModel>(funcionalidade);

                if (funcionalidade == null)
                {

                    string message = string.Format(Resource.NaoEncontradoParametro, Resource.Funcionalidade);
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }

                return funcionalidadeViewModel;
            }
            catch (RequisicaoException e)
            {

                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e)
            {

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Cadastra um novo funcionalidade.
        /// </summary>
        /// <param name="funcionalidadeViewModel"></param>
        /// <returns></returns>
        [ValidateModelStateFilter]
        public FuncionalidadeViewModel Post([FromBody]FuncionalidadeViewModel funcionalidadeViewModel)
        {
            try
            {
                var funcionalidade = Mapper.Map<FuncionalidadeViewModel, Funcionalidade>(funcionalidadeViewModel);

                _funcionalidadeService.Add(funcionalidade);

                funcionalidadeViewModel = Mapper.Map<Funcionalidade, FuncionalidadeViewModel>(funcionalidade);

                return funcionalidadeViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Atualiza dados de um funcionalidade.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="funcionalidadeViewModel"></param>
        public FuncionalidadeViewModel Put(long id, [FromBody]FuncionalidadeViewModel funcionalidadeViewModel)
        {
            try
            {
                var funcionalidade = Mapper.Map<FuncionalidadeViewModel, Funcionalidade>(funcionalidadeViewModel);

                _funcionalidadeService.Update(id, funcionalidade);

                funcionalidadeViewModel = Mapper.Map<Funcionalidade, FuncionalidadeViewModel>(funcionalidade);

                return funcionalidadeViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Realiza a exlusao de um registro
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            try
            {
                _funcionalidadeService.Remove(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}
