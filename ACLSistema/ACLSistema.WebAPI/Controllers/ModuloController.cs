﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.DomainViewModel.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using ACLSistema.WebAPI.Filters;
using ACLSistema.WebAPI.Models.Exceptions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ACLSistema.WebAPI.Controllers
{
    public class ModuloController : ApiController
    {
        private readonly IModuloService _moduloService;
        
        public ModuloController(IModuloService moduloService)
        {
            _moduloService = moduloService;            
        }

        /// <summary>
        /// Retorna uma lista de registros a partir de um filtro
        /// </summary>
        /// <param name="filtroViewModel"></param>
        /// <returns></returns>
        public List<ModuloViewModel> Get([FromUri]FiltroModuloViewModel filtroViewModel)
        {
            try
            {
                var filtro = Mapper.Map<FiltroModuloViewModel, FiltroModulo>(filtroViewModel); 

                var modulos = _moduloService.GetByFilter(filtro);

                var modulosViewModel = Mapper.Map<List<Modulo>, List<ModuloViewModel>>(modulos);

                if (modulos.Count == 0)
                {
                    string message = string.Format(Resource.NaoEncontradosParametros, Resource.Modulos);
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }

                return modulosViewModel;
            }
            catch (RequisicaoException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Retorna um registro a partir do parametro informado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public ModuloViewModel Get(long id)
        {
            try {

                var modulo = _moduloService.GetById(id);
                var moduloViewModel = Mapper.Map<Modulo, ModuloViewModel>(modulo);

                if (modulo == null) {

                    string message = string.Format(Resource.NaoEncontradoParametro, Resource.Modulo);                   
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }

                return moduloViewModel;
            }
            catch (RequisicaoException e) {
                                
                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e) {

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }            
        }

        /// <summary>
        /// Cadastra um novo modulo.
        /// </summary>
        /// <param name="moduloViewModel"></param>
        /// <returns></returns>
        [ValidateModelStateFilter]
        public ModuloViewModel Post([FromBody]ModuloViewModel moduloViewModel)
        {
            try
            {
                var modulo = Mapper.Map<ModuloViewModel, Modulo>(moduloViewModel);

                _moduloService.Add(modulo);

                moduloViewModel = Mapper.Map<Modulo, ModuloViewModel>(modulo);
                               
                return moduloViewModel;
            }
            catch (Exception e)
            {                
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Atualiza dados de um modulo.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moduloViewModel"></param>
        public ModuloViewModel Put(long id, [FromBody]ModuloViewModel moduloViewModel)
        {
            try
            {
                var modulo = Mapper.Map<ModuloViewModel, Modulo>(moduloViewModel);

                _moduloService.Update(id, modulo);

                moduloViewModel = Mapper.Map<Modulo, ModuloViewModel>(modulo);

                return moduloViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Realiza a exlusao de um registro
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            try
            {               
                _moduloService.Remove(id);               
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}
