﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.DomainViewModel.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using ACLSistema.WebAPI.Filters;
using ACLSistema.WebAPI.Models.Exceptions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ACLSistema.WebAPI.Controllers
{
    public class GrupoUsuarioController : ApiController
    {
        private readonly IGrupoUsuarioService _grupoUsuarioService;

        public GrupoUsuarioController(IGrupoUsuarioService grupoUsuarioService)
        {
            _grupoUsuarioService = grupoUsuarioService;
        }

        /// <summary>
        /// Retorna uma lista de registros a partir de um filtro
        /// </summary>
        /// <param name="filtroViewModel"></param>
        /// <returns></returns>
        public List<GrupoUsuarioViewModel> Get([FromUri]FiltroGrupoUsuarioViewModel filtroViewModel)
        {
            try
            {
                var filtro = Mapper.Map<FiltroGrupoUsuarioViewModel, FiltroGrupoUsuario>(filtroViewModel);

                var grupoUsuarios = _grupoUsuarioService.GetByFilter(filtro);

                var grupoUsuariosViewModel = Mapper.Map<List<GrupoUsuario>, List<GrupoUsuarioViewModel>>(grupoUsuarios);

                if (grupoUsuarios == null)
                {                    
                    string message = string.Format(Resource.NaoEncontradosParametros, Resource.Modulos);
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }

                return grupoUsuariosViewModel;
            }
            catch (RequisicaoException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Retorna um registro a partir do parametro informado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public GrupoUsuarioViewModel Get(long id)
        {
            try
            {
                var grupoUsuario = _grupoUsuarioService.GetById(id);
                var grupoUsuarioViewModel = Mapper.Map<GrupoUsuario, GrupoUsuarioViewModel>(grupoUsuario);

                if (grupoUsuario == null)
                {                    
                    string message = string.Format(Resource.NaoEncontradoParametro, Resource.GrupoUsuario);
                    throw new RequisicaoException(HttpStatusCode.NotFound, message);
                }
                return grupoUsuarioViewModel;
            }
            catch (RequisicaoException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(e.StatusCode, e.Message));
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Cadastra um novo grupoUsuario.
        /// </summary>
        /// <param name="grupoUsuarioViewModel"></param>
        /// <returns></returns>
        [ValidateModelStateFilter]
        public GrupoUsuarioViewModel Post([FromBody]GrupoUsuarioViewModel grupoUsuarioViewModel)
        {
            try
            {
                var grupoUsuario = Mapper.Map<GrupoUsuarioViewModel, GrupoUsuario>(grupoUsuarioViewModel);

                _grupoUsuarioService.Add(grupoUsuario);

                grupoUsuarioViewModel = Mapper.Map<GrupoUsuario, GrupoUsuarioViewModel>(grupoUsuario);

                return grupoUsuarioViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Atualiza dados de um grupoUsuario.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="grupoUsuarioViewModel"></param>
        public GrupoUsuarioViewModel Put(long id, [FromBody]GrupoUsuarioViewModel grupoUsuarioViewModel)
        {
            try
            {
                var grupoUsuario = Mapper.Map<GrupoUsuarioViewModel, GrupoUsuario>(grupoUsuarioViewModel);

                _grupoUsuarioService.Update(id, grupoUsuario);

                grupoUsuarioViewModel = Mapper.Map<GrupoUsuario, GrupoUsuarioViewModel>(grupoUsuario);

                return grupoUsuarioViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        /// <summary>
        /// Realiza a exlusao de um registro
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            try
            {
                _grupoUsuarioService.Remove(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}
