﻿using AutoMapper;

namespace ACLSistema.WebAPI.Mappers
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ViewModelToModelMappingProfile>();
                x.AddProfile<ModelToViewModelMappingProfile>();
            });
        }
    }
}