﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.DomainViewModel.Filtros;
using AutoMapper;

namespace ACLSistema.WebAPI.Mappers
{
    public class ModelToViewModelMappingProfile : Profile
    {
        protected void Configure()
        {
            // Entidades
            CreateMap<Funcionalidade, FuncionalidadeViewModel>();
            CreateMap<GruposUsuariosFuncionalidades, GruposUsuariosFuncionalidadesViewModel>();
            CreateMap<GruposUsuariosModulos, GruposUsuariosModulosViewModel>();
            CreateMap<GrupoUsuario, GrupoUsuarioViewModel>();
            CreateMap<Modulo, ModuloViewModel>();
            CreateMap<Usuario, UsuarioViewModel>();

            //Filtros
            CreateMap<FiltroModulo, FiltroModuloViewModel>();
            CreateMap<FiltroGrupoUsuario, FiltroGrupoUsuarioViewModel>();
        }
    }
}