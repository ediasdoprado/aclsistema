﻿using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.DomainViewModel.Entidades;
using ACLSistema.DomainViewModel.Filtros;
using AutoMapper;

namespace ACLSistema.WebAPI.Mappers
{
    public class ViewModelToModelMappingProfile : Profile
    {
        protected void Configure()
        {
            //Entidades
            CreateMap<FuncionalidadeViewModel, Funcionalidade>();
            CreateMap<GruposUsuariosFuncionalidadesViewModel, GruposUsuariosFuncionalidades>();
            CreateMap<GruposUsuariosModulosViewModel, GruposUsuariosModulos>();
            CreateMap<GrupoUsuarioViewModel, GrupoUsuario>();
            CreateMap<ModuloViewModel, Modulo>();
            CreateMap<UsuarioViewModel, Usuario>();

            //Filtros
            CreateMap<FiltroModuloViewModel, FiltroModulo>();
            CreateMap<FiltroGrupoUsuarioViewModel, FiltroGrupoUsuario>();
        }
    }
}