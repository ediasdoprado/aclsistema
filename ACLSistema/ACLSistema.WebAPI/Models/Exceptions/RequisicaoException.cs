﻿using System;
using System.Net;

namespace ACLSistema.WebAPI.Models.Exceptions
{

    public class RequisicaoException : Exception
    {
        public string Message { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public RequisicaoException(HttpStatusCode statusCode, string message)
        {
            Message = message;
            StatusCode = statusCode;
        }
    }
} 