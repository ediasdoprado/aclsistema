﻿using ACLSistema.Infra;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;

namespace ACLSistema.WebAPI.Filters
{
    public class ValidateModelStateFilter : ActionFilterAttribute
    {
        private List<string> _errors { get; set; }


        public ValidateModelStateFilter()
        {
            _errors = new List<string>();
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            foreach (var item in actionContext.ActionArguments)
            {
                if ((item.Key != null) && (item.Value == null))
                {                    
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, ValidationErrors(item.Key, Resource.ObjetoNulo));
                }
            }
            
            if (!actionContext.ModelState.IsValid)
            {                
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, ValidationErrors(actionContext));
            }
        }

        private Dictionary<string, string> ValidationErrors(string key, string mensagem) {

            Dictionary<string, string> validationErrors = new Dictionary<string, string>();

            validationErrors.Add(key, mensagem);

            return validationErrors;
        }

        private Dictionary<string, string> ValidationErrors(HttpActionContext actionContext)
        {

            Dictionary<string, string> validationErrors = new Dictionary<string, string>();

            foreach (var modelStateKey in actionContext.ModelState.Keys)
            {
                var modelStateVal = actionContext.ModelState[modelStateKey];
                foreach (var error in modelStateVal.Errors)
                {
                    var key = modelStateKey;
                    var errorMessage = error.ErrorMessage;
                    string[] field = key.Split('.');
                    if (!validationErrors.ContainsKey(field[1]))
                    {
                        validationErrors.Add(field[1], errorMessage);
                    }
                    else
                    {
                        validationErrors[field[1]] = errorMessage;
                    }
                }
            }

            return validationErrors;
        }

        //private List<string> ResponseMessage(HttpActionContext actionContext)
        //{

        //    List<string> modelStateErrors = new List<string>();
        //    foreach (ModelState modelState in actionContext.ModelState.Values)
        //    {
        //        foreach (ModelError error in modelState.Errors)
        //        {
        //            modelStateErrors.Add(error.ErrorMessage);
        //        }
        //    }

        //    return modelStateErrors;
        //}
    }
}