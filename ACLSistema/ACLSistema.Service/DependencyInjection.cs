﻿using ACLSistema.Data.IRepositories;
using ACLSistema.Data.Repositories;
using ACLSistema.Service.EntitiesServices;
using ACLSistema.Service.IEntitiesServices;
using SimpleInjector;

namespace ACLSistema.Service
{
    /// <summary>
    /// Gerenciamento das dependencias Interface x Classe
    /// </summary>
    public class DependencyInjection
    {
        /// <summary>
        /// Gerenciamento de dependencias dos repositorios.
        /// </summary>
        /// <param name="container"></param>
        public void GetContainerRepositories(Container container)
        {
            container.Register<IFuncionalidadeRepository, FuncionalidadeRepository>(Lifestyle.Singleton);
            container.Register<IGrupoUsuarioRepository, GrupoUsuarioRepository>(Lifestyle.Singleton);
            container.Register<IModuloRepository, ModuloRepository>(Lifestyle.Singleton);
        }

        /// <summary>
        /// Gerenciamento de dependencias dos servicos.
        /// </summary>
        /// <param name="container"></param>
        public void GetContainerServices(Container container)
        {
            container.Register<IFuncionalidadeService, FuncionalidadeService>(Lifestyle.Singleton);
            container.Register<IGrupoUsuarioService, GrupoUsuarioService>(Lifestyle.Singleton);
            container.Register<IModuloService, ModuloService>(Lifestyle.Singleton);
        }
    }
}
