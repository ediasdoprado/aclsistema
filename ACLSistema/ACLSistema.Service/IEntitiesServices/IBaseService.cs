﻿
using ACLSistema.Domain.Filtros;
using System.Collections.Generic;

namespace ACLSistema.Service.IEntitiesServices
{
    /// <summary>
    /// Interface com os servicos basicos compartilhados.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IBaseService<TEntity> where TEntity : class
    {
        void Add(TEntity obj);

        TEntity GetById(long id);
         
        List<TEntity> GetAll();

        void Update(long id, TEntity obj);

        void Remove(long id);

        List<TEntity> GetByFilter(FiltroBase<TEntity> filter);
    }
}
