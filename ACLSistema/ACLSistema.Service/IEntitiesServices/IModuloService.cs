﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Service.IEntitiesServices
{
    public interface IModuloService : IBaseService<Modulo>
    {        
    }
}
