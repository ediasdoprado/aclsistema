﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Service.IEntitiesServices
{
    public interface IGrupoUsuarioService : IBaseService<GrupoUsuario>
    {

    }
}
