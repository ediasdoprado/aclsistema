﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Service.IEntitiesServices
{
    public interface IFuncionalidadeService : IBaseService<Funcionalidade>
    {

    }
}
