﻿using System;
using System.Reflection;

namespace ACLSistema.Service.EntitiesServices
{
    /// <summary>
    /// Implementa servicos que podem ser usados por classes filhas.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class BaseService<TEntity> where TEntity : class
    {
        /// <summary>
        /// Seta os atributos necessarios a realizacao de exclusao logica.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity Exclusao(TEntity entity) {            
                       
            Type type = entity.GetType();

            var propertyAtivo = "Ativo";
            PropertyInfo propertyInfo = type.GetProperty(propertyAtivo);
            propertyInfo.SetValue(entity, false);

            var propertyExcluido = "Excluido";
            propertyInfo = type.GetProperty(propertyExcluido);
            propertyInfo.SetValue(entity, true);

            var propertyExclusao = "Exclusao";
            propertyInfo = type.GetProperty(propertyExclusao);
            propertyInfo.SetValue(entity, DateTime.Now);

            return entity;
        }

        /// <summary>
        /// Preenche dados obrigatorios para que um registro tenha sua exclusao logica removida.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TEntity Recadastro(TEntity entity) {

            Type type = entity.GetType();

            var propertyAtivo = "Ativo";
            PropertyInfo propertyInfo = type.GetProperty(propertyAtivo);
            propertyInfo.SetValue(entity, true);

            var propertyExcluido = "Excluido";
            propertyInfo = type.GetProperty(propertyExcluido);
            propertyInfo.SetValue(entity, false);

            var propertyExclusao = "Exclusao";
            propertyInfo = type.GetProperty(propertyExclusao);
            propertyInfo.SetValue(entity, null);

            return entity;
        }
    }
}
