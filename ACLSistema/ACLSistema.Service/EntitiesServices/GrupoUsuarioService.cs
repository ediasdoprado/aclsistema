﻿using ACLSistema.Data.IRepositories;
using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACLSistema.Service.EntitiesServices
{
    public class GrupoUsuarioService : BaseService<GrupoUsuario>, IGrupoUsuarioService
    {
        /// <summary>
        /// Instancia do repositorio de grupoUsuarios.
        /// </summary>
        private readonly IGrupoUsuarioRepository _grupoUsuarioRepository;

        public GrupoUsuarioService(IGrupoUsuarioRepository grupoUsuarioRepository)
        {
            _grupoUsuarioRepository = grupoUsuarioRepository;
        }

        /// <summary>
        /// Insere um novo registro.
        /// </summary>
        /// <param name="grupoUsuario"></param>
        public void Add(GrupoUsuario grupoUsuario)
        {
            try
            {
                var filter = new FiltroGrupoUsuario(grupoUsuario.Nome);

                var existeRegistro = GetByFilter(filter);

                if (existeRegistro.Count == 0)
                {
                    _grupoUsuarioRepository.Add(grupoUsuario);
                }
                else
                {
                    var registro = existeRegistro.FirstOrDefault();

                    if (registro.Excluido)
                    {
                        Recadastro(registro);
                        Update(registro.Id, registro);
                    }
                    else
                    {
                        var exception = string.Format(Resource.JaExistente, Resource.GrupoUsuario);
                        throw new Exception(exception);
                    }
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna uma lista de registros.
        /// </summary>
        /// <returns></returns>
        public List<GrupoUsuario> GetAll()
        {
            try
            {
                var grupoUsuarios = _grupoUsuarioRepository.GetAll();
                return grupoUsuarios;
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Retorna uma lista de registros de acordo com filtro.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<GrupoUsuario> GetByFilter(FiltroBase<GrupoUsuario> filter)
        {
            try
            {
                var grupoUsuarios = _grupoUsuarioRepository.GetByFilter(filter);
                return grupoUsuarios;
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Retorna um registro a partir de um id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GrupoUsuario GetById(long id)
        {
            try
            {
                var grupoUsuario = _grupoUsuarioRepository.GetById(id);

                return grupoUsuario;
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }

        public void Remove(long id)
        {
            try
            {
                var grupoUsuario = GetById(id);

                Exclusao(grupoUsuario);

                _grupoUsuarioRepository.Update(grupoUsuario);
            }
            catch (Exception e)
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Atualiza um registro.
        /// </summary>
        /// <param name="grupoUsuario"></param>
        public void Update(long id, GrupoUsuario grupoUsuario)
        {
            try
            {
                grupoUsuario.Id = id;

                var filter = new FiltroGrupoUsuario(grupoUsuario.Nome);

                var existeRegistro = GetByFilter(filter);

                var grupoUsuarioOriginal = GetById(id);

                if ((existeRegistro.Count == 0) || (grupoUsuarioOriginal.Id == existeRegistro.FirstOrDefault().Id))
                {
                    _grupoUsuarioRepository.Update(grupoUsuario);
                }
                else
                {
                    var exception = string.Format(Resource.JaExistente, Resource.GrupoUsuario);
                    throw new Exception(exception);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
