﻿using ACLSistema.Data.IRepositories;
using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACLSistema.Service.EntitiesServices
{
    /// <summary>
    /// Servicos responsaveis pelo CRUD de Funcionalidades.
    /// </summary>
    public class FuncionalidadeService : BaseService<Funcionalidade>, IFuncionalidadeService
    {
        /// <summary>
        /// Instancia do repositorio de Funcionalidades.
        /// </summary>
        private readonly IFuncionalidadeRepository _funcionalidadeRepository;

        public FuncionalidadeService(IFuncionalidadeRepository funcionalidadeRepository)
        {
            _funcionalidadeRepository = funcionalidadeRepository;
        }

        /// <summary>
        /// Insere um novo registro.
        /// </summary>
        /// <param name="funcionalidade"></param>
        public void Add(Funcionalidade funcionalidade)
        {
            try
            {

                var filter = new FiltroFuncionalidade(funcionalidade.Nome);

                var existeRegistro = GetByFilter(filter);

                if (existeRegistro.Count == 0)
                {

                    _funcionalidadeRepository.Add(funcionalidade);
                }
                else
                {

                    var registro = existeRegistro.FirstOrDefault();

                    if (registro.Excluido)
                    {
                        Recadastro(registro);
                        Update(registro.Id, registro);
                    }
                    else
                    {

                        var exception = string.Format(Resource.JaExistente, Resource.Funcionalidade);
                        throw new Exception(exception);
                    }
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna uma lista de registros.
        /// </summary>
        /// <returns></returns>
        public List<Funcionalidade> GetAll()
        {
            try
            {
                var funcionalidades = _funcionalidadeRepository.GetAll();
                return funcionalidades;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna uma lista de registros de acordo com filtro.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Funcionalidade> GetByFilter(FiltroBase<Funcionalidade> filter)
        {
            try
            {
                var funcionalidades = _funcionalidadeRepository.GetByFilter(filter);
                return funcionalidades;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna um registro a partir de um id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Funcionalidade GetById(long id)
        {
            try
            {
                var funcionalidade = _funcionalidadeRepository.GetById(id);

                return funcionalidade;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Realiza remoção logica.
        /// </summary>
        /// <param name="id"></param>
        public void Remove(long id)
        {
            try
            {
                var funcionalidade = GetById(id);

                Exclusao(funcionalidade);

                _funcionalidadeRepository.Update(funcionalidade);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Atualiza um registro.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="funcionalidade"></param>
        public void Update(long id, Funcionalidade funcionalidade)
        {
            try
            {
                funcionalidade.Id = id;

                var filter = new FiltroFuncionalidade(funcionalidade.Nome);

                var existeRegistro = GetByFilter(filter);

                var funcionalidadeOriginal = GetById(id);

                if ((existeRegistro.Count == 0) || (funcionalidadeOriginal.Id == existeRegistro.FirstOrDefault().Id))
                {
                    _funcionalidadeRepository.Update(funcionalidade);
                }
                else
                {
                    var exception = string.Format(Resource.JaExistente, Resource.Funcionalidade);
                    throw new Exception(exception);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
