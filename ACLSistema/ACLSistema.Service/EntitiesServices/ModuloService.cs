﻿using ACLSistema.Data.IRepositories;
using ACLSistema.Domain.Entidades;
using ACLSistema.Domain.Filtros;
using ACLSistema.Infra;
using ACLSistema.Service.IEntitiesServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACLSistema.Service.EntitiesServices
{
    /// <summary>
    /// Servicos responsaveis pelo CRUD de Modulos.
    /// </summary>
    public class ModuloService : BaseService<Modulo>, IModuloService
    {
        /// <summary>
        /// Instancia do repositorio de modulos.
        /// </summary>
        private readonly IModuloRepository _moduloRepository;

        public ModuloService(IModuloRepository moduloRepository)
        {
            _moduloRepository = moduloRepository;
        }

        /// <summary>
        /// Insere um novo registro.
        /// </summary>
        /// <param name="modulo"></param>
        public void Add(Modulo modulo)
        {
            try {

                var filter = new FiltroModulo(modulo.Nome);

                var existeRegistro = GetByFilter(filter);

                if (existeRegistro.Count == 0) {

                    _moduloRepository.Add(modulo);
                }
                else {

                    var registro = existeRegistro.FirstOrDefault();

                    if (registro.Excluido)
                    {
                        Recadastro(registro);
                        Update(registro.Id, registro);
                    }
                    else {

                        var exception = string.Format(Resource.JaExistente, Resource.Modulo);
                        throw new Exception(exception);
                    }
                }
            }
            catch (Exception e) {

                throw new Exception(e.Message);
            }            
        }

        /// <summary>
        /// Retorna uma lista de registros.
        /// </summary>
        /// <returns></returns>
        public List<Modulo> GetAll()
        {
            try
            {
                var modulos = _moduloRepository.GetAll();
                return modulos;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna uma lista de registros de acordo com filtro.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<Modulo> GetByFilter(FiltroBase<Modulo> filter)
        {
            try
            {
                var modulos = _moduloRepository.GetByFilter(filter);
                return modulos;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retorna um registro a partir de um id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Modulo GetById(long id)
        {            
            try
            {
                var modulo = _moduloRepository.GetById(id);

                return modulo;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Realiza remoção logica.
        /// </summary>
        /// <param name="id"></param>
        public void Remove(long id)
        {
            try
            {
                var modulo = GetById(id);

                Exclusao(modulo);               

                _moduloRepository.Update(modulo);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Atualiza um registro.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modulo"></param>
        public void Update(long id, Modulo modulo)
        {
            try
            {
                modulo.Id = id;

                var filter = new FiltroModulo(modulo.Nome);

                var existeRegistro = GetByFilter(filter);

                var moduloOriginal = GetById(id);

                if ((existeRegistro.Count == 0) || (moduloOriginal.Id == existeRegistro.FirstOrDefault().Id))
                {
                    _moduloRepository.Update(modulo);
                }
                else
                {
                    var exception = string.Format(Resource.JaExistente, Resource.Modulo);
                    throw new Exception(exception);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
