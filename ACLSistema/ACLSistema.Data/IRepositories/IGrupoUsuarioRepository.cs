﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Data.IRepositories
{
    public interface IGrupoUsuarioRepository : IBaseRepository<GrupoUsuario>
    {

    }
}
