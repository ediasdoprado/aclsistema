﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Data.IRepositories
{
    public interface IUsuarioRepository : IBaseRepository<Usuario>
    {

    }
}
