﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Data.IRepositories
{ 
    public interface IModuloRepository : IBaseRepository<Modulo>
    {

    }
}
