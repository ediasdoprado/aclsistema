﻿using ACLSistema.Domain.Filtros;
using System.Collections.Generic;

namespace ACLSistema.Data.IRepositories
{
    /// <summary>
    /// Interface com os metodos do repositorio base.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity obj);

        TEntity GetById(long id);

        List<TEntity> GetAll();

        TEntity Update(TEntity obj);

        void Remove(TEntity obj);

        List<TEntity> GetByFilter(FiltroBase<TEntity> filter);        
    }
}
