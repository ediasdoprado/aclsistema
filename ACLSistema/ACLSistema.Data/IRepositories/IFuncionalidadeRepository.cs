﻿using ACLSistema.Domain.Entidades;

namespace ACLSistema.Data.IRepositories
{
    public interface IFuncionalidadeRepository : IBaseRepository<Funcionalidade>
    {

    }
}
