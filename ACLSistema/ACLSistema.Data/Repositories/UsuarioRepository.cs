﻿using ACLSistema.Data.IRepositories;
using ACLSistema.Domain.Entidades;

namespace ACLSistema.Data.Repositories
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {

    }
}
