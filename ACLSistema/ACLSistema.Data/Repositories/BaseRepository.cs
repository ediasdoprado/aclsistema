﻿using ACLSistema.Domain.Filtros;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ACLSistema.Data.Repositories
{
    public abstract class BaseRepository<TEntity> where TEntity : class
    {
        protected EFContext _efContext = new EFContext();

        public TEntity Add(TEntity obj)
        {
            _efContext.Set<TEntity>().Add(obj);
            _efContext.SaveChanges();
            return obj;
        }

        public TEntity GetById(long id)
        {
            _efContext.Configuration.AutoDetectChangesEnabled = false;
            _efContext.Set<TEntity>().AsNoTracking();
            return _efContext.Set<TEntity>().Find(id);
        }

        public List<TEntity> GetAll()
        {
            _efContext.Configuration.AutoDetectChangesEnabled = false;
            return _efContext.Set<TEntity>().AsNoTracking().ToList();
        }

        public TEntity Update(TEntity obj)
        {
            _efContext.Entry(obj).State = EntityState.Modified;
            _efContext.SaveChanges();
            return obj;
        }

        public void Remove(TEntity obj)
        {
            _efContext.Entry(obj).State = EntityState.Deleted;
            _efContext.Set<TEntity>().Remove(obj);
            _efContext.SaveChanges();
        }

        public List<TEntity> GetByFilter(FiltroBase<TEntity> filter)
        {
            var query = _efContext.Set<TEntity>().AsNoTracking().Where(filter.predicate.Compile());
            var quantidadeRegistros = query.Count();
            var registros = query.Skip((filter.pagina - 1) * filter.registros).Take(filter.registros);
            return registros.ToList();
        }
    }
}
