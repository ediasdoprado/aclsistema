﻿using ACLSistema.Domain.Entidades;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace ACLSistema.Data
{
    public class EFContext : DbContext
    {
        public DbSet<Modulo> Modulo { get; set; }
                
        public EFContext(): base("aclsistemadb")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            
            Configuration.ValidateOnSaveEnabled = false;
            Configuration.ProxyCreationEnabled = false;  
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Funcionalidade>();
            modelBuilder.Entity<GrupoUsuario>();
            //modelBuilder.Entity<GruposUsuariosFuncionalidades>();
            //modelBuilder.Entity<GruposUsuariosModulos>();
            modelBuilder.Entity<Modulo>();
            //modelBuilder.Entity<Usuario>();
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("Cadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Cadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("Cadastro").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("Ativo") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Ativo").CurrentValue = true;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("Codigo") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Codigo").CurrentValue = Guid.NewGuid();
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("Codigo").IsModified = false;
                }
            }

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("Atualizacao") != null))
            {
                if (entry.State == EntityState.Modified)
                {
                    entry.Property("Atualizacao").CurrentValue = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}
